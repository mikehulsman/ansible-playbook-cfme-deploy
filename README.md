Role Name
=========

This role deploy's CloudForms version 5.9.2.4-1
Depending on the supplied parameters it can be an all in one deployment or a multi region, multizone with stand-alone, external or HA Database


Not in scope
------------
This will not (yet) deploy the cmfe appliance onto an infrastucture, cloud or container platform.


Requirements
------------

An already installed cmfe appliance, or multiple appliances.
If extra disk for tmp or databases are needed, they must be defined in that specific hostvars. (it cannot dertermine the disk by itself)


Role Variables
--------------

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.
cfme_deploy_dbpass
cfme_deploy_dbserver
cfme_deploy_region
cfme_deploy_initialpasswd

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

GPL

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
